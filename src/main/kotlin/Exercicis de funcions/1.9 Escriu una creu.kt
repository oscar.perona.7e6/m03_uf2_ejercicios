package `Exercicis de funcions`

import java.util.*

fun main (){
    val scanner = Scanner(System.`in`)
    println("Introduzca el tamaño y el caracterde la cruz: ")
    val CrossSize = scanner.nextInt()
    val CrossChar = scanner.next().single()

    // Tamaño mínimo de la cruz
    if (CrossSize >= 3) {
        println(cross(CrossSize,CrossChar))
    } else{
        println("El tamaño de la cruz a de ser igual o menor que 3")
    }
}

fun cross (CrossSize:Int,CrossChar:Char){

    for(i in 0 until CrossSize){
        if(i !=CrossSize /2) {
            for(j in 1..CrossSize /2) print(" ")
            print(CrossChar)
        }
        else {
            for(j in 1..CrossSize) print(CrossChar)
        }
        println()
    }
}

