package `Exercicis de funcions`

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un string y un caracter:")
    val imputString = scanner.nextLine()
    val imputChar = scanner.nextLine().single()

    println(stringList(imputString, imputChar))
}
fun stringList(imputString: String, imputChar: Char):MutableList<String>{

    val listOfString = mutableListOf<String>()
    var stringToAdd = ""

    for (i in imputString.indices){
        if (imputString[i] != imputChar) stringToAdd+="${imputString[i]}"
        else {
           listOfString.add(stringToAdd)
           stringToAdd = ""
        }
    }
    if (stringToAdd != "") listOfString.add(stringToAdd)

    return listOfString
}