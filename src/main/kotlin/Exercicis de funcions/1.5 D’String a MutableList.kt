package exercicisDeFuncions

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un string:")
    val imputString = scanner.nextLine()

    println(charList(imputString))
}
fun charList(imputString: String):MutableList<Char>{
    val listOfChar = mutableListOf<Char>()
    for (i in imputString) listOfChar.add(i)
    return listOfChar
}
