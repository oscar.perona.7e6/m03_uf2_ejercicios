package exercicisDeFuncions

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un string:")
    val firstString = scanner.next()
    val secondString = scanner.next()

    println(equalString(firstString, secondString))
}
fun equalString(firstString:String, secondString:String):Boolean{
    return firstString == secondString
}
