package exercicisDeFuncions

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un string y una posicio:")
    val imputString = scanner.nextLine()
    val stringPosition = scanner.nextInt()

    println(findCharacter(imputString, stringPosition))
}
fun findCharacter(imputString: String, stringPosition: Int):String{
    if (imputString.length >= stringPosition) return imputString[stringPosition].toString()
    else return "La mida de l'String és inferior a $stringPosition"
}
