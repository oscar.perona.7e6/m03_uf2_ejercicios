package exercicisDeFuncions

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un string y un inici y final:")
    val imputString = scanner.nextLine()
    val start = scanner.nextInt()
    val end = scanner.nextInt()

    println(subsequencia(imputString, start, end))
}
fun subsequencia(imputString: String, start:Int, end:Int):String{
    var result = ""
    if (imputString.length > start && imputString.length >= end){
        for (i in imputString.indices){
            if (i in start until end) result +=imputString[i]
        }
        return result
    }
    else return "La subseqüència $start-$end de l'String no existe"
}
