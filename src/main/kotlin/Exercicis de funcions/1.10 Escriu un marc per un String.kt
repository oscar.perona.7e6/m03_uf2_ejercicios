package `Exercicis de funcions`

import java.util.*

fun main (){
    val scanner = Scanner(System.`in`)
    println("Introduzca un nombre y un caracter ")
    val Imputword = scanner.next()
    val MarkChar = scanner.next().single()
    println(mark(Imputword, MarkChar))
}

fun mark(Imputword: String, MarkChar: Char){
    println("$MarkChar".repeat(Imputword.length + 4))
    println("$MarkChar $Imputword $MarkChar")
    println("$MarkChar".repeat(Imputword.length + 4))
}
