package `2 - Exercicis de recursivitat`

import java.util.*

fun main (){
    val scanner = Scanner(System.`in`)
    println("Escribe el numero del cual quieras calcular el doble factorial")
    println("El resultado es ${dobleFactorial(scanner.nextLong())}")
}

fun dobleFactorial(n:Long):Long{
    return if (n>1) n*dobleFactorial(n-2)
    else 1
}

