package `Exercicis de recursivitat`

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introduce un numero:")
    val imputInt = scanner.nextInt()
    println("${numerosCrecientes(imputInt.toString())}")

}


fun numerosCrecientes(n:String):Boolean{
    for (i in 0 until n.lastIndex){
        if (n[i]<=n[i+1])return numerosCrecientes(n.drop(1))
        else return false
    }
    return true
}


