package `2 - Exercicis de recursivitat`

import java.util.*

fun main (){
    val scanner = Scanner(System.`in`)
    println("Escribe el numero del cual quieras calcular el factorial")
    println("El resultado es ${factorial(scanner.nextLong())}")
}

fun factorial(n: Long): Long{
    if (n > 1) return n * factorial(n-1)
    else return 1
}

