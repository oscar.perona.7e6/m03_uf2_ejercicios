package `Exercicis de recursivitat`
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introduce un número:")
    val num = scanner.nextInt()
    println(reduccionDigitos(num))
}
fun reduccionDigitos (n:Int) :Int{
    if (n < 10) return n
    else return reduccionDigitos(n%10 + reduccionDigitos(n/10))
}

