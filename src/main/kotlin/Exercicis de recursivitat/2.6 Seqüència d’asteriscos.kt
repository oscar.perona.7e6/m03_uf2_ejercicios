package `Exercicis de recursivitat`
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introduce un número:")
    val firstNumber = scanner.nextInt()
    print(secubeciaAsteriscos(firstNumber))
}

fun secubeciaAsteriscos (n:Int) {
    if (1 < n){
        secubeciaAsteriscos(n-1)
    }
    for (i in 1 .. n){
        print("*")
    }
    println()
    if (n>1){
        secubeciaAsteriscos(n-1)
    }
}
