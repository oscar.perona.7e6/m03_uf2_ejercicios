package `Exercicis de recursivitat`
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introduce un numero")
    val number = scanner.nextInt()
    var divisores=0
    var result= primersPerfectes(number)
    if (result/10 != 0){
        result= primersPerfectes(result)
    }
    if (result/10 == 0){
        for (i in 1..result){
            if (result%i ==0){
                divisores++
            }
        }
        if (divisores==2){
            println(true)
        }
        else {
            println(false)}
    }
}

fun primersPerfectes(n:Int):Int{
    var result:Int
    if (n!=0){
        result= n%10 + primersPerfectes(n/10)
    }
    else{
        return 0
    }
    return result
}
