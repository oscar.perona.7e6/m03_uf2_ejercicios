package `2 - Exercicis de recursivitat`

import java.util.*

fun main() {
    val scanner= Scanner(System.`in`)
    println("Introdueix un número:")
    println(digitCount(scanner.nextInt()))
}
fun digitCount (n:Int):Int{
    if (n<10) return 1
    else return 1 + digitCount(n/10)
}
